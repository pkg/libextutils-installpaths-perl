libextutils-installpaths-perl (0.014-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 03:58:27 +0000

libextutils-installpaths-perl (0.014-1) unstable; urgency=medium

  * Import upstream version 0.014.
  * Make build and runtime dependency on libextutils-config-perl
    versioned.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Sep 2024 18:16:57 +0200

libextutils-installpaths-perl (0.013-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on debhelper.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 0.013.
  * Remove Contact field from debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Apr 2024 21:10:50 +0200

libextutils-installpaths-perl (0.012-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 23:01:28 +0000

libextutils-installpaths-perl (0.012-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 29 Dec 2020 01:07:57 +0100

libextutils-installpaths-perl (0.012-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 00:55:37 +0000

libextutils-installpaths-perl (0.012-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Import upstream version 0.012
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 May 2018 15:10:35 +0200

libextutils-installpaths-perl (0.011-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.011
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Florian Schlichting ]
  * Fix autopkgtest: Smoke testing t/destinations.t needs to find blib/bin and
    blib/script directories in the testbed
  * Declare compliance with Debian Policy 3.9.8

 -- Florian Schlichting <fsfs@debian.org>  Wed, 27 Apr 2016 22:29:14 +0200

libextutils-installpaths-perl (0.010-1) unstable; urgency=low

  * Team upload

  * Imported Upstream version 0.010
  * drop trailing slash from metacpan URLs
  * claim conformance with Policy 3.9.5

 -- Damyan Ivanov <dmn@debian.org>  Fri, 08 Nov 2013 19:03:43 +0200

libextutils-installpaths-perl (0.009-1) unstable; urgency=low

  * Initial release (closes: #714378).

 -- gregor herrmann <gregoa@debian.org>  Fri, 28 Jun 2013 17:07:37 +0200
